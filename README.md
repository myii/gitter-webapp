# Gitter

Unofficial webapp for Gitter, an open-source instant messaging and chat room system for developers and users of GitHub repositories.

## Interface

Swipe _anywhere_ on the app to interact with the _main menu_:

* Swipe right to _reveal_ it.
* Swipe left to _hide_ it.
